%global _empty_manifest_terminate_build 0
Name:		python-zope-deprecation
Version:	5.0
Release:	1
Summary:	Zope Deprecation Infrastructure
License:	ZPL 2.1
URL:		https://github.com/zopefoundation/zope.deprecation
Source0:	https://github.com/zopefoundation/zope.deprecation/archive/%{version}.tar.gz
BuildArch:	noarch

%description
This package provides a simple function called deprecated(names, reason) to mark deprecated
modules, classes, functions, methods and properties.

%package -n python3-zope-deprecation
Summary:	Zope Deprecation Infrastructure
Provides:	python-zope-deprecation
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-zope-deprecation
This package provides a simple function called deprecated(names, reason) to mark deprecated
modules, classes, functions, methods and properties.

%package help
Summary:	Development documents and examples for zope.deprecation
Provides:	python3-zope-deprecation-doc

%description help
Development documents and examples for zope.deprecation.

%prep
%autosetup -n zope.deprecation-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-zope-deprecation -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_pkgdocdir}

%changelog
* Mon Apr 10 2023 wubijie <wubijie@kylinos.cn> - 5.0-1
- Update package to version 5.0

* Thu Dec 17 2020 Python_Bot <Python_Bot@openeuler.org> - 4.4.0-1
- Package Spec generated
